import styled from "@emotion/styled";
import { MasterLayout } from "../../components/templates/MasterLayout"

const CartPageStyled = styled.div`
padding: 40px 0;
  .heading{
    color: #707070;
    margin-bottom: 40px;
    font-size: 1.5rem;
    font-weight: 700;
  }
`;

const CartPage = ()=>{
  return(
    <MasterLayout activeButton="giohang">
      <CartPageStyled>
        <div className="container">
          <div className="heading">Giỏ hàng của bạn</div>
          <div className="text-center">Hiện tại giỏ hàng đang trống.</div>
        </div>
      </CartPageStyled>
    </MasterLayout>
  )
}
export default CartPage;