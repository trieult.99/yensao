import styled from "@emotion/styled";
import { MasterLayout } from "../components/templates/MasterLayout";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faClock, faUser } from "@fortawesome/free-regular-svg-icons";
import Button from "../components/atoms/Button";

const ContactPageStyled = styled.div`
  padding: 0 0 40px;
  .header{
    background-color: #FFF6E3;
    padding: 40px 0;
    text-align: center;
    h1{
      color: #E2B769;
      font-size: 3.5rem;
      font-weight: 700;
      margin-bottom: 24px;
    }
    .updateInfo{
      display: flex;
      justify-content: center;
      color: #707070;
    }
  }
  .content{
    color: #252525;
    padding: 60px 0;
    .companyInfo{
      div{
        margin-bottom: 10px;
      }
      .name{
        font-size: 1.75rem;
        font-weight: 700;
        margin-bottom: 20px;
      }
    }
    .contactForm{
      background-color: #FFF6E3;
      border: 1px solid #cecece;
      border-radius: 20px;
      padding: 40px 60px;
      text-align: center;
      &_title{
        font-size: 1.25rem;
        font-weight: 700;
        margin-bottom: 20px;
      }
      input, textarea{
        width: 100%;
        margin-bottom: 16px;
        border: 1px solid #C8C8C8;
        border-radius: 4px;
        padding: 8px 16px;
      }
    }
  }
`;

const ContactPage = ()=>{
  return(
    <MasterLayout activeButton="lienhe">
      <ContactPageStyled>
        <div className="header">
          <div className="container">
            <h1>Thông tin liên hệ</h1>
            <div className="updateInfo">
              <div className="me-4"><FontAwesomeIcon icon={faClock}/> 20/06/2022</div>
              <div><FontAwesomeIcon icon={faUser}/> Xuân Trường</div>
            </div>
          </div>
        </div>
        <div className="content">
          <div className="container">
            <div className="row">
              <div className="col-6">
                <div className="companyInfo">
                  <div className="name">Yến Sào Phương Nam</div>
                  <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                  <div><b>Địa chỉ: </b> Số 2 phường 3 Hồ Chí Minh</div>
                  <div><b>Website: </b> https://yensaophuongnam.com</div>
                  <div><b>Googlemap: </b> https://yensaophuongnam.com</div>
                  <div><b>Facebook: </b> https://yensaophuongnam.com</div>
                  <div><b>Instagram: </b> https://yensaophuongnam.com</div>
                  <div><b>Thời gian làm việc: </b> 7:00 - 16:30 từ thứ 2 đến chủ nhật</div>
                </div>
              </div>
              <div className="offset-1 col-5">
                <div className="contactForm">
                  <div className="contactForm_title">Liên hệ với chúng tôi</div>
                  <form action="">
                    <input type="text" placeholder="Họ tên *" />
                    <input type="text" placeholder="Email *" />
                    <input type="text" placeholder="Số điện thoại *" />
                    <textarea placeholder="Nội dung (Nếu có)" />
                    <Button className="w-100">Gửi thông tin</Button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </ContactPageStyled>
    </MasterLayout>
  )
}
export default ContactPage;