import styled from '@emotion/styled';
import type { NextPage } from 'next';
import Image from 'next/image';
import Button from '../components/atoms/Button';
import { MasterLayout } from '../components/templates/MasterLayout';
import { Swiper, SwiperSlide } from 'swiper/react';
import "swiper/css";
import "swiper/css/bundle";
import { Autoplay, Pagination } from "swiper";
import ProductCard from '../components/molecules/card/ProductCard';
import ArticleCard from '../components/molecules/card/ArticleCard';

const HomePageStyled = styled.div`
  .homeBanner{
    background-color: #FFF6E3;
    padding: 60px 0 160px;
    h1{
      color: #BF4434;
      font-weight: 700;
      margin-bottom: 16px;
      font-size: 3.5rem;
    }
    p{
      color: #707070;
      font-size: 1.25rem;
      margin-bottom: 32px;
    }
  }
  .contactPhone{
    &_container{
      background-color: white;
      border-radius: 30px;
      position: absolute;
      transform: translateY(-50%);
      left: 0;
      right: 0;
      top: 0;
      padding: 40px 80px;
      display: flex;
      justify-content: space-between;
      align-items: center;
      box-shadow: 0px 10px 30px rgba(0, 0, 0, 0.1);
    }
    &_content{
      h2{
        color: #BF4434;
        font-weight: 700;
        font-size: 1.75rem;
      }
      p{
        color: #707070;
        font-size: 1.25rem;
      }
    }
    &_phoneBtn{
      background: #BF4434;
      border-radius: 20px;
      padding: 18px;
      display: flex;
      color: white;
      font-size: 1.25rem;
    }
  }
  .promotionsSlider{
    padding: 175px 0 0;
    .swiper-slide{
      margin-bottom: 40px;
      img {
        display: block;
        object-fit: cover;
        border-radius: 20px;
      }
    }
    .swiper-pagination{
      bottom: 0;
      .swiper-pagination-bullet{
        width: 16px;
        height: 16px;
        margin: 0 8px;
        background-color: #a5a5a5;
        &-active{
          background-color: #BF4434;
        }
      }
    }
  }
  .featuredProducts{
    padding: 70px 0 80px;
    &_heading{
      display: flex;
      justify-content: space-between;
      margin-bottom: 25px;
      align-items: baseline;
      .heading_title{
        font-weight: 700;
        font-size: 1.75rem;
      }
      .heading_seeAll{
        color: #707070;
        text-decoration: underline;
      }
    }
    .col-3{
      padding: 0;
    }
  }
  .articles{
    padding: 40px 0;
    background-color: #FFF6E3;
    &_heading{
      display: flex;
      justify-content: space-between;
      margin-bottom: 25px;
      align-items: baseline;
      .heading_title{
        font-weight: 700;
        font-size: 1.75rem;
      }
      .heading_seeAll{
        color: #707070;
        text-decoration: underline;
      }
    }
    .col-4{
      padding: 0;
    }
  }
  .contactPhoneForm{
    background-color: #BF4434;
    color: white;
    padding: 114px 0;
    form{
      h3{
        font-weight: 700;
        margin-bottom: 24px;
      }
      p{
        line-height: 1.8em;
        margin-bottom: 32px;
      }
      .formSubmit{
        display: flex;
        input{
          width: 100%;
          background: linear-gradient(109.46deg, rgba(201, 201, 201, 0.8) 1.57%, rgba(196, 196, 196, 0.1) 100%);
          opacity: 0.6;
          border-radius: 30px;
          border: none;
          padding: 10px 20px;
          color: white;
          ::placeholder {
            color: white;
            font-style: italic;
          }
        }
      }
    }
  }
`;

const Home: NextPage = () => {
  return (
    <MasterLayout>
      <HomePageStyled>
        <section className='homeBanner'>
          <div className='container'>
            <div className='row'>
              <div className='col-6'>
                <div className='homeBanner_content'>
                  <h1>Yến Sào Khánh Hòa</h1>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>              
                  <Button type='cta'>Liên hệ tư vấn</Button>
                </div>
              </div>
              <div className='offset-1 col-5'>
                <div className='homeBanner_coverImg'>
                  <Image src="/images/homebanner.png" width={520} height={370} alt='' />
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className='contactPhone'>
          <div className='container position-relative'>
            <div className='contactPhone_container'>
              <div className='contactPhone_content'>
                <h2>Yến sào Phương Nam - Yến thật 100%</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit Lorem ipsum dolor si</p>
              </div>
              <div className='contactPhone_phoneBtn'>
                <a href="tel:18007923887">
                  <div className='d-flex align-items-center'>
                    <Image src="/images/phoneicon.png" width={80} height={80} alt='' />
                    <div className='mx-4'>
                      <div className='fw-bold'>Liên hệ ngay</div>
                      <div>1800 792 3887</div>
                    </div>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </section>
        <section className='promotionsSlider'>
          <div className='container'>
            <Swiper
            pagination={true}
            loop={true}
            autoplay={{
              delay: 4000,
              disableOnInteraction: false,
            }}
            modules={[Pagination, Autoplay]}
            >
              <SwiperSlide>
                <Image src="/images/promotioncover.png" width={1320} height={342} alt='' />
              </SwiperSlide>
              <SwiperSlide>
                <Image src="/images/promotioncover.png" width={1320} height={342} alt='' />
              </SwiperSlide>
              <SwiperSlide>
                <Image src="/images/promotioncover.png" width={1320} height={342} alt='' />
              </SwiperSlide>
            </Swiper>
          </div>
        </section>
        <section className='featuredProducts'>
          <div className='container'>
            <div className='featuredProducts_heading'>
              <div className='heading_title'>Sản phẩm nổi bật</div>
              <div className='heading_seeAll'>Xem tất cả</div>
            </div>
            <div className='row'>
              <div className='col-3'>
                <ProductCard
                  img='/images/productyensao.png'
                  name='Yến sào Khánh Hoà'
                  price='3.000.000'
                  weight='500gr'
                />
              </div>
              <div className='col-3'>
                <ProductCard
                  img='/images/productyensao.png'
                  name='Yến sào Khánh Hoà'
                  price='3.000.000'
                  weight='500gr'
                />
              </div>
              <div className='col-3'>
                <ProductCard
                  img='/images/productyensao.png'
                  name='Yến sào Khánh Hoà'
                  price='3.000.000'
                  weight='500gr'
                />
              </div>
              <div className='col-3'>
                <ProductCard
                  img='/images/productyensao.png'
                  name='Yến sào Khánh Hoà'
                  price='3.000.000'
                  weight='500gr'
                />
              </div>
            </div>
          </div>
        </section>
        <section className='articles'>
        <div className='container'>
            <div className='articles_heading'>
              <div className='heading_title'>Kiến thức</div>
              <div className='heading_seeAll'>Xem tất cả</div>
            </div>
            <div className='row'>
              <div className='col-4'>
                <ArticleCard
                  img='/images/howtos.png'
                  name='Cách chế biến yến bổ dưỡng'
                  summary='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. '
                />
              </div>
              <div className='col-4'>
                <ArticleCard
                  img='/images/howtos.png'
                  name='Cách chế biến yến bổ dưỡng'
                  summary='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. '
                />
              </div>
              <div className='col-4'>
                <ArticleCard
                  img='/images/howtos.png'
                  name='Cách chế biến yến bổ dưỡng'
                  summary='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. '
                />
              </div>
            </div>
          </div>
        </section>
        <section className='contactPhoneForm'>
          <div className='container'>
            <div className='row'>
              <div className='offset-2 col-5'>
                <form action="">
                  <h3>Liên hệ tư vấn</h3>
                  <p>Learn more about Startup Framework in the light demo version. It has components from the full version, two great samples and documentation. You can also find two images of a Macbook and an iPad, which you can use in your.</p>
                  <div className='formSubmit'>
                    <input type="text" placeholder='Nhập số điện thoại của bạn' />
                    <Button type='outline' className='ms-4'>Gửi</Button>
                  </div>
                </form>
              </div>
              <div className='offset-1 col-2'>
                <Image src={'/images/logosquare.png'} width={212} height={228} alt=''/>
              </div>
            </div>
          </div>
        </section>
      </HomePageStyled>
    </MasterLayout>
  );
};

export default Home;
