/* eslint-disable react/no-unescaped-entities */
import React, { CSSProperties, useState } from 'react';

import styled from '@emotion/styled';
import { MasterLayout } from '../../components/templates/MasterLayout';
import { useRouter } from 'next/router';
import { Swiper, SwiperSlide } from 'swiper/react';
// Import Swiper styles
import 'swiper/css';
import 'swiper/css/free-mode';
import 'swiper/css/navigation';
import 'swiper/css/thumbs';

// import required modules
import { FreeMode, Navigation, Thumbs } from 'swiper';
import Image from 'next/image';
import Button from '../../components/atoms/Button';

const ProductsDetailPageStyled = styled.div`
  img {
    object-fit: cover;
  }
  .imgAndPrice {
    padding: 40px 0;
    .imgSelected {
      margin-bottom: 24px;
      line-height: 0;
      border-radius: 20px;
      .swiper-slide span {
        width: 100% !important;
      }
    }
    .imgList {
      .swiper-slide {
        line-height: 0;
        span {
          width: 100% !important;
        }
        img {
          border-radius: 14px;
          cursor: pointer;
        }
      }
    }
    .price {
      background-color: #fff6e3;
      padding: 32px;
      border-radius: 20px;
    }

    .summary {
      margin-top: 45px;
      &_title {
        font-size: 1.125rem;
        font-weight: 700;
        margin-bottom: 16px;
      }
      &_content {
        line-height: 1.75em;
      }
    }
  }
  .description {
    background-color: #fff6e3;
    padding: 50px 0 300px;
    &_title {
      font-size: 1.125rem;
      font-weight: 700;
      margin-bottom: 24px;
    }
    &_content {
      img {
        line-height: 0;
        border-radius: 14px;
        width: 100%;
      }
    }
  }
`;
const ProductsDetailPage = () => {
  const router = useRouter();
  const slug = router.query.slug;
  const [thumbsSwiper, setThumbsSwiper] = useState<any>(null);

  return (
    <MasterLayout activeButton="sanpham">
      <ProductsDetailPageStyled>
        <section className="imgAndPrice">
          <div className="container">
            <div className="row">
              <div className="col-lg-6">
                <Swiper
                  style={
                    {
                      '--swiper-navigation-color': '#fff',
                      '--swiper-pagination-color': '#fff',
                    } as CSSProperties
                  }
                  spaceBetween={6}
                  navigation={true}
                  thumbs={{
                    swiper:
                      thumbsSwiper && !thumbsSwiper.destroyed
                        ? thumbsSwiper
                        : null,
                  }}
                  modules={[FreeMode, Navigation, Thumbs]}
                  className="imgSelected"
                >
                  {Array.from(Array(6), () => (
                    <SwiperSlide>
                      <Image
                        width={620}
                        height={400}
                        alt=""
                        src="/images/productImg.png"
                      />
                    </SwiperSlide>
                  ))}
                </Swiper>
                <Swiper
                  onSwiper={setThumbsSwiper}
                  spaceBetween={24}
                  slidesPerView={4}
                  freeMode={true}
                  watchSlidesProgress={true}
                  modules={[FreeMode, Navigation, Thumbs]}
                  className="imgList"
                >
                  {Array.from(Array(6), () => (
                    <SwiperSlide>
                      <Image
                        width={140}
                        height={140}
                        alt=""
                        src="/images/productImg.png"
                      />
                    </SwiperSlide>
                  ))}
                </Swiper>
              </div>
              <div className="col-lg-6 ps-lg-5">
                <div className="price mt-lg-0 mt-4">
                  <h1 className="mb-4 fontColor-yellow h3 fw-bold">
                    Yến sào Khánh Hoà loại 1
                  </h1>

                  <div className="d-flex">
                    <label className="w-75 me-5">
                      <div className="mb-2">Khối lượng</div>
                      <select className="form-select">
                        <option selected>Open this select menu</option>
                        <option value="1">500</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                      </select>
                    </label>
                    <label className="w-25">
                      <div className="mb-2">Số lượng</div>
                      <input className="form-control" type="number" />
                    </label>
                  </div>

                  <hr className="my-4 fontColor-yellow" />
                  <h3 className="fontColor-yellow fw-bold mb-4">3.000.000</h3>
                  <div className="d-flex">
                    <Button type="default" className="w-100 me-4">
                      Mua ngay
                    </Button>
                    <Button type="outline" className="w-100">
                      Thêm vào giỏ
                    </Button>
                  </div>
                </div>
                <div className="summary">
                  <div className="summary_title">Mô tả ngắn:</div>
                  <p className="summary_content">
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry. Lorem Ipsum has been the industry's
                    standard dummy text ever since the 1500s, when an unknown
                    printer took a galley of type and scrambled it to make a
                    type specimen book. It has survived not only five t has
                    survived not
                  </p>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className="description">
          <div className="container">
            <div className="description_title">Thông tin sản phẩm</div>
            <div className="description_content">
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                sunt in culpa qui officia deserunt mollit anim id est
                laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                sed do eiusmod tempor incididunt ut labore et dolore magna
                aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis
                aute irure dolor in reprehenderit in voluptate velit esse cillum
                dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                cupidatat non proident, sunt in culpa qui officia deserunt
                mollit anim id est laborum.
              </p>
              <img
                alt=""
                src="/images/productDescriptionImg.png"
                className="my-5"
              />
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                sunt in culpa qui officia deserunt mollit anim id est
                laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                sed do eiusmod tempor incididunt ut labore et dolore magna
                aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis
                aute irure dolor in reprehenderit in voluptate velit esse cillum
                dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                cupidatat non proident, sunt in culpa qui officia deserunt
                mollit anim id est laborum.
              </p>
            </div>
          </div>
        </section>
      </ProductsDetailPageStyled>
    </MasterLayout>
  );
};
export default ProductsDetailPage;
