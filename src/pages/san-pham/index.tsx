import styled from '@emotion/styled';
import ProductCard from '../../components/molecules/card/ProductCard';
import { MasterLayout } from '../../components/templates/MasterLayout';

const ProductsPageStyled = styled.div`
  padding: 40px 0;
  .productGroup {
    margin-bottom: 40px;
    .categoryName {
      font-size: 1.5rem;
      font-weight: 700;
      margin-bottom: 40px;
      color: #707070;
    }
  }
`;

const ProductsPage = () => {
  return (
    <MasterLayout activeButton="sanpham">
      <ProductsPageStyled>
        <div className="container">
          <div className="productGroup">
            <div className="categoryName">Yến tinh chế</div>
            <div className="productList">
              <div className="row">
                {Array.from(Array(6), () => (
                  <div className="col-3 mb-5">
                    <ProductCard
                      img="/images/productyensao.png"
                      name="Yến sào Khánh Hoà"
                      price="3.000.000"
                      weight="500gr"
                    />
                  </div>
                ))}
              </div>
            </div>
          </div>

          <div className="productGroup">
            <div className="categoryName">Yến thô</div>
            <div className="productList">
              <div className="row">
                {Array.from(Array(3), () => (
                  <div className="col-3 mb-5">
                    <ProductCard
                      img="/images/productyensao.png"
                      name="Yến sào Khánh Hoà"
                      price="3.000.000"
                      weight="500gr"
                    />
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </ProductsPageStyled>
    </MasterLayout>
  );
};
export default ProductsPage;
