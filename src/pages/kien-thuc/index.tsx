import styled from "@emotion/styled";
import ArticleCard from "../../components/molecules/card/ArticleCard";
import { MasterLayout } from "../../components/templates/MasterLayout";

const ArticlesPageStyled = styled.div`
  padding: 40px 0;
`;

const ArticlesPage = ()=>{
  return(
    <MasterLayout activeButton="kienthuc">
      <ArticlesPageStyled>
        <div className="container">
          <div className="articleList">
            <div className="row">
              {Array.from(Array(6), () => (
                <div className="col-4 mb-5">
                  <ArticleCard
                    img='/images/howtos.png'
                    name='Cách chế biến yến bổ dưỡng'
                    summary='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. '
                  />
                </div>
              ))}
            </div>
          </div>
        </div>
      </ArticlesPageStyled>
    </MasterLayout>
  )
}
export default ArticlesPage;