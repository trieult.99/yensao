/* eslint-disable react/no-unescaped-entities */
import { useRouter } from 'next/router';
import { MasterLayout } from '../../components/templates/MasterLayout';
import { useState } from 'react';
import Image from 'next/image';
import styled from '@emotion/styled';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faClock, faUser } from "@fortawesome/free-regular-svg-icons";
import { faShareNodes } from '@fortawesome/free-solid-svg-icons';

const ArticlesDetailPageStyled = styled.div`
  .header {
    background-color: #fff6e3;
    text-align: center;
    padding: 330px 0 50px;
    &_info {
      display: flex;
      justify-content: center;
      color: #707070;
      div:not(:last-child) {
        margin-right: 18px;
      }
    }
    &_img {
      position: absolute;
      top: 0;
      left: 50%;
      transform: translateX(-50%);
    }
    h1 {
      color: #e2b769;
      text-shadow: 0px 4px 10px rgba(226, 183, 105, 0.4);
      font-weight: 700;
      margin-bottom: 24px;
    }
  }
  .content {
    padding: 45px 0;
    color: #252525;
    line-height: 180%;
    text-align: justify;
    img {
      width: 100vw !important;
    }
    >*{
      margin-bottom: 35px !important;
    }
  }
`;

const ArticlesDetailPage = () => {
  const router = useRouter();
  const slug = router.query.slug;
  const [thumbsSwiper, setThumbsSwiper] = useState<any>(null);

  return (
    <MasterLayout activeButton="kienthuc">
      <ArticlesDetailPageStyled>
        <div className="header">
          <div className="header_img">
            <Image width={454} height={399} alt="" src="/images/light.png" />
          </div>
          <h1>Impact of Accessibility of Handy Services</h1>
          <div className="header_info">
            <div><FontAwesomeIcon icon={faClock}/>: 20/06/2022</div>
            <div><FontAwesomeIcon icon={faUser}/>: Admin</div>
            <div><FontAwesomeIcon icon={faShareNodes} />: Share</div>
          </div>
        </div>
        <div className="container">
          <div className="content">
            <p>
              Take a deep breath and Imagine yourself in a world where you wake
              up at 7:30am and have to be at work by 8:00am but the sad part is
              that you live in Nigeria's commercial hub and traffic is now your
              extra- curricular activity. However, In this world, your pseudo
              suit has been neatly ironed and waiting to be worn. There's
              breakfast in bed too(and if you want, there's someone you can call
              on to feed you.) The best part? there's an automobile that encodes
              your destination and teleports you to work. The impact is that you
              get to work before Ms Helen who woke up at 4:00am and had to fix
              hotdogs for breakfast; the same you woke up to meet smoky, hot and
              greasy. While she had to sieve her way through traffic and almost
              had a fight with the danfo driver, you were still on your bed,
              dreaming about your crush and smiling sheepishly. What's worse?
              Your Boss tells everyone about how they should be like you. He
              tells them that with people like Helen, the company will come to
              dust but with hardworking and punctual people like you, the
              company is set for great things. You are happy and suddenly feel
              like how a teenager feels when the Principal fixes the senior
              prefect badge on the breast pocket of his crispy ironed packet
              white. Helen on the other hand, walks into the office stealthily
              like a child hiding from the wrath of her father. But you are not
              really the best. You are not really a punctual person either.
              However, the deal is, you have accessibility to quality and handy
              services.
            </p>
            <Image
              width={1096}
              height={500}
              alt=""
              src="/images/articleDetailImg.png"
            />
            <p>
              Take a deep breath and Imagine yourself in a world where you wake
              up at 7:30am and have to be at work by 8:00am but the sad part is
              that you live in Nigeria's commercial hub and traffic is now your
              extra- curricular activity. However, In this world, your pseudo
              suit has been neatly ironed and waiting to be worn. There's
              breakfast in bed too(and if you want, there's someone you can call
              on to feed you.) The best part? there's an automobile that encodes
              your destination and teleports you to work. The impact is that you
              get to work before Ms Helen who woke up at 4:00am and had to fix
              hotdogs for breakfast; the same you woke up to meet smoky, hot and
              greasy. While she had to sieve her way through traffic and almost
              had a fight with the danfo driver, you were still on your bed,
              dreaming about your crush and smiling sheepishly. What's worse?
              Your Boss tells everyone about how they should be like you. He
              tells them that with people like Helen, the company will come to
              dust but with hardworking and punctual people like you, the
              company is set for great things. You are happy and suddenly feel
              like how a teenager feels when the Principal fixes the senior
              prefect badge on the breast pocket of his crispy ironed packet
              white. Helen on the other hand, walks into the office stealthily
              like a child hiding from the wrath of her father. But you are not
              really the best. You are not really a punctual person either.
              However, the deal is, you have accessibility to quality and handy
              services.
            </p>
          </div>
        </div>
      </ArticlesDetailPageStyled>
    </MasterLayout>
  );
};
export default ArticlesDetailPage;
