/* eslint-disable @next/next/no-img-element */
import styled from "@emotion/styled";
import Button from "../../atoms/Button";
import NextLink from 'next/link';

const ProductCardStyled = styled.div`
  padding: 16px;
  display: flex;
  flex-direction: column;
  align-items: center;
  border: 1px solid transparent;
  cursor: pointer;
  img{
    width: 100%;
    border-radius: 10px;
    object-fit: cover;
  }
  .productName, .productPrice, .productWeight{
    color: #E2B769;
    font-size: 1.5rem;
  }
  .productName{
    margin-top: 16px;
    margin-bottom: 4px;
  }
  .productPrice{
    font-weight: 700;
  }
  &:hover{
    background-color: #FFF1D6;
    border: 1px solid #E2B769;
    border-radius: 20px;
    box-shadow: 0px 6px 30px rgba(226, 183, 105, 0.5);
  }
`

export interface ProductCardProps {
  img: string
  name: string
  price: string
  weight: string
}

export const ProductCard = (
  props: ProductCardProps
)=>{
  return(
    <NextLink
      href={"/san-pham/23453/yen-sao-khanh-hoa-loai-1"}
    >
      <ProductCardStyled>
        <img className="productImg" src={props.img} width={270} height={270} alt=''/>
        <div className="productName">{props.name}</div>
        <div>
          <span className="productPrice">{props.price}</span>
          <span className="productWeight">&nbsp;({props.weight})</span>
        </div>
        <Button className="mt-3" size="small" onClick={(event)=>event.stopPropagation()}>Thêm vào giỏ</Button>
      </ProductCardStyled>
    </NextLink>
  )
}
export default ProductCard;