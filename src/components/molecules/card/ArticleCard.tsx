/* eslint-disable @next/next/no-img-element */
import styled from '@emotion/styled';
import Button from '../../atoms/Button';
import NextLink from 'next/link';

const ArticleCardStyled = styled.div`
  padding: 16px;
  display: flex;
  flex-direction: column;
  align-items: center;
  border: 1px solid transparent;
  cursor: pointer;
  .img {
    width: 100%;
    padding: 24px 16px 16px;
    background-color: rgba(234, 208, 161, 0.5);
    border-radius: 6px;
    position: relative;
    .square {
      /* position: absolute; */
      position: absolute;
      height: 8px;
      width: 8px;
      background-color: #e2b769;
      border-radius: 1px;
      top: 8px;
      /* left: 16px; */
      &::before {
        content: '';
        position: absolute;
        height: 8px;
        width: 8px;
        background-color: #e2b769;
        border-radius: 1px;
        left: 12px;
      }
      &::after {
        content: '';
        position: absolute;
        height: 8px;
        width: 8px;
        background-color: #e2b769;
        border-radius: 1px;
        left: 24px;
      }
    }
    img {
      width: 100%;
      border-radius: 4px;
      object-fit: cover;
    }
  }
  .name {
    color: #e2b769;
    font-size: 1.5rem;
    margin-top: 24px;
    font-weight: 700;
  }
  .summary {
    font-size: 0.875rem;
    margin-top: 10px;
    color: #707070;
    text-align: center;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    line-clamp: 2;
    -webkit-box-orient: vertical;
  }
  &:hover {
    background: #fff1d6;
    border: 1px solid #e2b769;
    border-radius: 20px;
    box-shadow: 0px 6px 30px rgba(226, 183, 105, 0.5);
  }
`;

export interface ArticleCardProps {
  img: string;
  name: string;
  summary: string;
}

export const ArticleCard = (props: ArticleCardProps) => {
  return (
    <NextLink href={'/kien-thuc/23453/cach-che-bien-yen-bo-duong'}>
      <ArticleCardStyled>
        <div className="img">
          <div className="square"></div>
          <img src={props.img} width={358} height={230} alt="" />
        </div>
        <div className="name">{props.name}</div>
        <p className="summary mb-0">{props.summary}</p>
      </ArticleCardStyled>
    </NextLink>
  );
};
export default ArticleCard;
