import { cx } from '@emotion/css';
import styled from '@emotion/styled';
import React, { MouseEvent, PropsWithChildren } from 'react';
import { respondTo } from '../../utils/breakpoints';

export interface ButtonProps {
  type?: 'default' | 'outline' | 'cta'
  size?: 'large' | 'small' | 'xsmall' | ''
  className?: string
  active?: boolean
  onClick?: (evt: MouseEvent<HTMLButtonElement>) => void
}

const ButtonStyled = styled.button`
  min-width: 8rem;
  font-size: inherit;
  border-radius: 100px;
  height: 48px;
  font-weight: 700;
  border: transparent;
  padding: 0 20px;
  cursor: pointer;
  &[data-size='large']{
    height: 48px;
  }
  &[data-size='small']{
    height: 40px;
    font-size: 0.875rem;
  }
  &[data-size='xsmall']{
    height: 32px;
  }
  &[data-type='default']{
    background-color: #BF4434;
    color: white;
    ${ respondTo.sm`
        &:hover{
          background-color: #D58B81;
        }   
        &:active{
          background-color: #A75146;
        }
    ` }       
  }
  &[data-type='outline']{
    background-color: white;
    color: #BF4434;
    border: 1px solid #BF4434 ;
    ${ respondTo.sm`
        &:hover{
          background-color: #EAD2CE;
        }   
        &:active{
          background-color: #D58B81;
        }
    ` }   
  }
  
  &[data-type='cta']{
    background-color: #BF4434;
    color: white;
    padding: 0 35px;
    height: 60px;
    box-shadow: 0px 4px 20px rgba(191, 68, 52, 0.4);
    ${ respondTo.sm`
        &:hover{
          background-color: #D58B81;
        }
        &:active{
          background-color: #A75146;
        }
    ` }  
  }
`

export const Button = ({
  type = 'default',
  size = '',
  className = '',
  onClick,
  active = false,
  children
}: PropsWithChildren<ButtonProps>)=>{
  return(
    <ButtonStyled
    className={className}
    data-active={ active }
    data-size={ size } 
    data-type={ type }
    onClick={ onClick }
    >
      {children}
    </ButtonStyled>
  )
}

export default Button;