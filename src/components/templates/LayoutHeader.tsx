import React from 'react';

import styled from '@emotion/styled';
import Image from 'next/image';
import { cx } from '@emotion/css';
import NextLink from 'next/link';


const LayoutHeaderStyled = styled.header`
  background-color: #FFF6E3;
  .navbar{
    padding: 30px 0;
    &_logo{
      height: auto;
      cursor: pointer;
    }
    &_links{
      display: flex;
      .navbarLink{
        padding: 10px 20px;
        border-radius: 50px;
        font-size: 0.875rem;
        letter-spacing: 0.05em;
        margin-left: 15px;
        &.-active{
          background-color: #BF4434;
          box-shadow: 0px 4px 20px rgba(191, 68, 52, 0.4);
          font-weight: 700;
          a{
            color: white;
          }
        }
      }
    }
  }
`;

export interface LayoutHeaderProps {
  activeButton?: 'trangchu' | 'sanpham' | 'kienthuc' | 'lienhe' | 'giohang'
}

export const LayoutHeader = ({
  activeButton = 'trangchu'
}: LayoutHeaderProps) => (
  <LayoutHeaderStyled>
    <div className='container'>
      <div className='navbar'>
        <NextLink href="/">
          <a>
            <Image className='navbar_logo' src="/images/logofull.png" alt="" width={100} height={40} />
          </a>
        </NextLink>
        <div className='navbar_links'>
          <div className={cx('navbarLink', {
            '-active': activeButton == 'trangchu'
          })}>
            <NextLink href="/">Trang chủ</NextLink>
          </div>
          <div className={cx('navbarLink', {
            '-active': activeButton == 'sanpham'
          })}>
            <NextLink href="/san-pham">Sản phẩm</NextLink>
          </div>
          <div className={cx('navbarLink', {
            '-active': activeButton == 'kienthuc'
          })}>
            <NextLink href="/kien-thuc">Kiến thức</NextLink>
          </div>
          <div className={cx('navbarLink', {
            '-active': activeButton == 'lienhe'
          })}>
            <NextLink href="/lien-he">Liên hệ</NextLink>
          </div>
          <div className={cx('navbarLink', {
            '-active': activeButton == 'giohang'
          })}>
            <NextLink href="/gio-hang">Giỏ hàng</NextLink>
          </div>
        </div>
      </div>
    </div>
  </LayoutHeaderStyled>
);

export default LayoutHeader;